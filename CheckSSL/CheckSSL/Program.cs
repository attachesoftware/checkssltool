﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;

namespace CheckSSL
{
    internal class Program
    {
        public const ConsoleColor defaultConsoleColor = ConsoleColor.Gray;

        private static void Main(string[] args)
        {
            string url = "";

            Console.Clear();

            /******************************************************************************/
            /******************************************************************************/
            if (args.Length > 0 && args.Any(x => x.ToUpper() == "/T"))
            {
                DisplayDetalsForCurrentTLS();

                ConsoleWriteLn("Press any key to exist.");

                Console.ReadLine();
                return;
            }
            /******************************************************************************/
            /******************************************************************************/

            if (args.Length > 1 && args[0].ToUpper() == "/U")
            {
                url = args[1];
            }
            else
            {
                url = GetURLFromRegistrySetting();
            }

            if (!args.Any(x => x.ToUpper() == "/T"))
                ChooseSecurityProtocol();

            ConsoleWriteLn($"Checking AttService URL -> {url} ");
            ConsoleWriteLn($"Security Protocol: {System.Net.ServicePointManager.SecurityProtocol.ToString().ToUpper()}{Environment.NewLine}", ConsoleColor.Blue);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                //request.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

                request.ServerCertificateValidationCallback += (sender, cert, chain, error) =>
                {
                    DisplayCertificateValidationResult(cert, error);

                    if (args.Length > 0 && args.Any(x => x.ToUpper() == "/C"))
                    {
                        DisplayCertificateChainDetails(chain);
                    }

                    return (error == SslPolicyErrors.None);
                };

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                ConsoleWriteLn("The connection with AttService looks OK", ConsoleColor.Green);
            }
            catch (Exception ex)
            {
                ConsoleWriteLn("The connection with AttService failed with the following errors:");
                ConsoleWriteLn("");
                ConsoleWriteLn(ex.Message, ConsoleColor.Red);
                ConsoleWriteLn(ex.InnerException?.Message, ConsoleColor.Red);
            }

            ConsoleWriteLn("");
            ConsoleWriteLn("Press any key to exist.");

            Console.ReadLine();
        }

        private static string GetURLFromRegistrySetting()
        {
            string url;
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey("Software\\Attache Software\\AttacheServer");
            var hostName = (string)regKey?.GetValue("HostName");
            regKey?.Close();
            url = $"https://{hostName}:{6587}/";
            return url;
        }

        private static void DisplayCertificateValidationResult(System.Security.Cryptography.X509Certificates.X509Certificate cert, SslPolicyErrors error)
        {
            ConsoleWriteLn("Certificate Validation Result:");
            ConsoleWriteLn($"Hash: {cert.GetCertHashString()}");
            ConsoleWriteLn($"Name: {cert.GetName()}");
            ConsoleWriteLn($"Issuer Name: {cert.GetIssuerName()}");
            ConsoleWriteLn($"Format: {cert.GetFormat()}");
            ConsoleWriteLn($"KeyAlgorithm: {cert.GetKeyAlgorithm()}");
            ConsoleWriteLn($"SerialNumber: {cert.GetSerialNumberString()}");

            ConsoleWriteLn($"SSLPolicyErrors: {error.ToString()}", ConsoleColor.Red);

            ConsoleWriteLn("");
        }

        private static void DisplayCertificateChainDetails(System.Security.Cryptography.X509Certificates.X509Chain chain)
        {
            ConsoleWriteLn("****************************************************");

            ConsoleWriteLn("Chain Information");
            ConsoleWriteLn($"Chain revocation flag: {chain.ChainPolicy.RevocationFlag}");
            ConsoleWriteLn($"Chain revocation mode: {chain.ChainPolicy.RevocationMode}");
            ConsoleWriteLn($"Chain verification flag: {chain.ChainPolicy.VerificationFlags}");
            ConsoleWriteLn($"Chain verification time: {chain.ChainPolicy.VerificationTime}");
            ConsoleWriteLn($"Chain status length: {chain.ChainStatus.Length}");
            ConsoleWriteLn($"Chain application policy count: {chain.ChainPolicy.ApplicationPolicy.Count}");
            ConsoleWriteLn($"Chain certificate policy count: {chain.ChainPolicy.CertificatePolicy.Count}");
            ConsoleWriteLn("");
            //Output chain element information.
            ConsoleWriteLn("Chain Element Information");
            ConsoleWriteLn($"Number of chain elements: {chain.ChainElements.Count}");
            ConsoleWriteLn($"Chain elements synchronized? {chain.ChainElements.IsSynchronized}");

            foreach (var element in chain.ChainElements)
            {
                ConsoleWriteLn($"Element issuer name: {element.Certificate.Issuer}");
                ConsoleWriteLn($"Element certificate valid until: {element.Certificate.NotAfter}");
                ConsoleWriteLn($"Element certificate is valid: {element.Certificate.Verify()}");
                ConsoleWriteLn($"Element error status length: {element.ChainElementStatus.Length}");
                ConsoleWriteLn($"Element information: {element.Information}");
                ConsoleWriteLn($"Number of element extensions: {element.Certificate.Extensions.Count}");

                if (chain.ChainStatus.Length > 1)
                {
                    for (int index = 0; index < element.ChainElementStatus.Length; index++)
                    {
                        ConsoleWriteLn(element.ChainElementStatus[index].Status.ToString());
                        ConsoleWriteLn(element.ChainElementStatus[index].StatusInformation);
                    }
                }
            }

            ConsoleWriteLn("****************************************************");
        }

        private static void DisplayDetalsForCurrentTLS()
        {
            ConsoleWriteLn("Getting the details of current TLS version...");

            try
            {
                var response = WebRequest.Create("https://www.howsmyssl.com/a/check").GetResponse();
                var responseData = new StreamReader(response.GetResponseStream()).ReadToEnd();
                ConsoleWriteLn(responseData.Replace(",", "," + Environment.NewLine).Replace("[", "[" + Environment.NewLine).Replace("]", Environment.NewLine + "]").Replace("{", "{" + Environment.NewLine).Replace("}", Environment.NewLine + "}"));
            }
            catch (Exception ex)
            {
                ConsoleWriteLn(ex.Message, ConsoleColor.Red);
            }
        }

        private static void ConsoleWriteLn(string text, ConsoleColor foreColor = defaultConsoleColor)
        {
            if (foreColor != defaultConsoleColor) Console.ForegroundColor = foreColor;
            else Console.ForegroundColor = defaultConsoleColor;
            Console.WriteLine(text);
        }

        private static void ChooseSecurityProtocol()
        {
            ConsoleWriteLn($"Current Security Protocol: {System.Net.ServicePointManager.SecurityProtocol.ToString().ToUpper()}", ConsoleColor.Blue);
            ConsoleWriteLn("Choose one of the following options, if you need to switch to a specific Security Protocol:");
            ConsoleWriteLn("");
            ConsoleWriteLn("1. TLS12 (1.2)");
            ConsoleWriteLn("2. TLS11 (1.1)");
            ConsoleWriteLn("3. TLS   (1.0)");
            ConsoleWriteLn("4. SSL3  (3.0)");
            ConsoleWriteLn("5. Auto Mode");
            ConsoleWriteLn("");
            ConsoleWriteLn("Press Enter to continue with current Security Protocol");

            while (true)
            {
                var keyInfo = Console.ReadKey(false);

                switch (keyInfo.KeyChar.ToString())
                {
                    case "1":
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        Console.Clear();
                        return;

                    case "2":
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                        Console.Clear();
                        return;

                    case "3":
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                        Console.Clear();
                        return;

                    case "4":
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                        Console.Clear();
                        return;

                    case "5":
                        System.Net.ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        Console.Clear();
                        return;

                    //case "5":
                    //    System.Net.ServicePointManager.SecurityProtocol = 0; // System Default
                    //    Console.Clear();
                    //    return;

                    case "\r":
                        Console.Clear();
                        return;

                    default:
                        continue;
                }
            }
        }
    }
}