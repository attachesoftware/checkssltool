# CheckSSLTool

Selecting Security Protocol:
We can select Security Protocol by entering 1,2,3,4 or 5 or pressing Enter to continue with current setting.
Note, auto-mode includes all the TLS versions when connecting to AttService

1: TLS12 (1.2)
2: TLS11 (1.1)
3: TLS   (1.0)
4: SSL3  (3.0)
5: Auto Mode


Optional Parameters:

"/t" : Shows some details about the TLS version used 
"/c" : Shows some details about certificate chain
"/u" + URL: We can provide custome URL (default one comes from Attache's registry setting)
